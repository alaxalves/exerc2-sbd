#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

typedef struct Automovel
{
    char marca[15];
    char modelo[15];
    char cor[15];
    char placa[7];
    char renavam[12];

} automovel;

typedef struct ProprietarioAutomoveis
{
    char nome[25];
    char telefone[15];
    char rg[9];
    char cpf[12];
    int quantCarros;
    automovel *automoveis;

} proprietarioAutomoveis;

int main()
{

    proprietarioAutomoveis donoCarro;

    int contador = 0;

    printf("-------------------------------------------------------\n");
    printf("-------------FORMULARIO DO PROPRIETARIO----------------\n");
    printf("-------------------------------------------------------\n");

    printf("Nome do Proprietário: ");
    //scanf("%s", &donoCarro.nome);
    fgets(donoCarro.nome, 25, stdin);
    fflush(stdin);

    printf("Telefone: ");
    //scanf("%s", &donoCarro.telefone);
    fgets(donoCarro.telefone, 15, stdin);
    fflush(stdin);

    printf("CPF: (sem caracteres especiais) ");
    fgets(donoCarro.cpf, 12, stdin);
    fflush(stdin);

    printf("RG: ");
    scanf("%s", donoCarro.rg);
    //fgets(donoCarro.rg, 9, stdin);

    printf("Digite a quantidade de carros a ser cadastrada: ");
    scanf("%d", &donoCarro.quantCarros);

    automovel carro[donoCarro.quantCarros];
    donoCarro.automoveis = malloc( donoCarro.quantCarros * sizeof(automovel));

    printf("\n");
    fflush(stdin);

    for(contador = 0; contador < donoCarro.quantCarros; contador ++)
    {
        printf("---------------------------------------------------------\n");
        printf("-----------------FORMULARIO DE AUTOMOVEL-----------------\n");
        printf("---------------------------------------------------------\n");

        printf("Marca do carro(ex: Chevrolet): ");
        scanf("%s", carro[contador].marca);

        printf("Modelo do Carro(ex: Fusca): ");
        scanf("%s", carro[contador].modelo);

        printf("Placa (ex: ABC1234): ");
        scanf("%s", carro[contador].placa);

        printf("Renavam: (ex: 01234567891)");
        scanf("%s", carro[contador].renavam);

        printf("Cor predominante(ex: Amarelo): ");
        scanf("%s", carro[contador].cor);
        //fgets(carro.categoria, 15, stdin);
        printf("\n\n\n\n");
    }
    printf("-----------------------------------------------------------\n");
    printf("--------------DADOS PREENCHIDOS DO FORMULARIO--------------\n");
    printf("-----------------------------------------------------------\n");
    printf("Proprietário: %s\n", donoCarro.nome);
    printf("CPF: %s\n", donoCarro.cpf);
    printf("RG: %s\n", donoCarro.rg);
    printf("Telefone: %s\n", donoCarro.telefone);

    for(contador = 0; contador < donoCarro.quantCarros; contador ++)
    {
        printf("Carro registrado: %s %s\n", carro[contador].marca, carro[contador].modelo);
        printf("Placa: %s \n", carro[contador].placa);
        printf("Renavam: %s\n", carro[contador].renavam);

    }

    FILE *criaFormularioDono = fopen("formulario_proprietarios.txt", "a");
    FILE *criaFormularioCarro = fopen("formulario_carros.txt", "a");


    if(criaFormularioDono == NULL)
    {
        printf("Não foi possível fazer o registro!");
    }
    else
    {
        fprintf(criaFormularioDono, "Proprietário: %s\n", donoCarro.nome);
        fprintf(criaFormularioDono, "Telefone: %s\n", donoCarro.telefone);
        fprintf(criaFormularioDono, "RG: %s\n", donoCarro.rg);
        fprintf(criaFormularioDono, "CPF: %s\n", donoCarro.cpf);
        fprintf(criaFormularioDono, "Quantidade de Veículos: %d\n", donoCarro.quantCarros);
        //int contador1 = 0;
        printf("\n\n");

    }
    if(criaFormularioCarro == NULL)
    {

    }
    else
    {
        for(contador = 0; contador < donoCarro.quantCarros; contador++)
        {
            fprintf(criaFormularioCarro, "Marca: %s\n", carro[contador].marca);
            fprintf(criaFormularioCarro, "Modelo: %s\n", carro[contador].modelo);
            fprintf(criaFormularioCarro, "Cor: %s\n", carro[contador].cor);
            fprintf(criaFormularioCarro, "Placa: %s\n", carro[contador].placa);
            fprintf(criaFormularioCarro, "Renavam: %s\n", carro[contador].renavam);
            fprintf(criaFormularioCarro, "-------Dados do Proprietario------\n");
            fprintf(criaFormularioCarro, "RG: %s\n", donoCarro.rg);
            fprintf(criaFormularioCarro, "CPF: %s\n", donoCarro.cpf);
        }

    }

    fclose(criaFormularioDono);
    fclose(criaFormularioCarro);
    return 0;
}
